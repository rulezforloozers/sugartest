<html>
<head>
	<title>App</title>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="main.js" type="text/javascript"></script>
	<link rel="stylesheet" href="main.css" type="text/css" />
</head>
<body>
	<div class="form">
		<form id="form" method="post" action="/">
				<div class="r">
					<label>ФИО</label>
					<input type="text" value="User1" name="name" />
					<div class="errMsg">Введите ФИО!</div>
				</div>

				<div class="r">
					<label>Сообщение</label>
					<textarea cols="10" rows="5" name="message">Message from User1</textarea>
					<div class="errMsg">Введите сообщение!</div>
				</div>

				<div class="r">
					<label>email-адрес</label>
					<input type="text" value="user1@rambler.ru" name="email" />
					<div class="errMsg">Введен неверный E-mail адрес!</div>
				</div>

				<br />
				<input type="reset" value="Очистить">
				<input type="submit" value="Отправить">
		</form>
	</div>

	<div id="captcha" class="captcha">
		<div class="b" id="popup">
			<div class="c"><img src="" id="captchaimg" /></div>
			<div class="i"><input type="text" value="" name="code" id="scode" /></div>
			<div id="cptmsg" class="errMsg"></div>
			<button onclick="send()">Отправить</button>
		</div>
	</div>

	<div id="popup2"></div>
</body>
</html>