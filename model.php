<?php
date_default_timezone_set('Europe/Kiev');
require_once 'db.php';

class Messages
{
	private $id;
	private $name;
	private $message;
	private $email;
	private $date_created;
	private $sent = 0;

	private $table = 'messages';
	private $error = 0;

	function __construct($data) {
		if(isset($data['id'])) {
			$this->id = $data['id'];
			$this->date_created = $data['date_created'];
			$this->sent = $data['sent'];
		} else {
			$this->id = GUID();
			$this->date_created = date("Y-m-d H:i:s");
		}

		$this->name = $data['name'];
		$this->message = $data['message'];
		$this->email = $data['email'];
	}

	public function getErrorCode() {
		return $this->error;
	}

	public function getName() {
		return $this->name;
	}

	public function getEmail() {
		return $this->email;
	}

	public function getMessage() {
		return $this->message;
	}

	public function setSent($sent) {
		$q = db::prepare('UPDATE '.$this->table.' SET sent = :sent WHERE id = :id');
		return $q->execute(array(':id' => $this->id, ':sent' => $sent));
	}

	private function validate() {
		if(empty($this->name)) {
			$this->error = 4;
			return false;
		}

		if(empty($this->message)) {
			$this->error = 5;
			return false;
		}

		if(empty($this->email)) {
			$this->error = 2;
			return false;
		}

		// security code
		if($_POST['captcha'] != $_POST['code']) {
			$this->error = 3;
			return false;
		}

		return true;
	}

	public function save() {
		if(!$this->validate()) return false;
		$q = db::prepare('INSERT INTO '.$this->table.' (id, name, message, email, date_created, sent) VALUES(:id, :name, :message, :email, :date_created, :sent)');
		$q->bindParam(':id', $this->id);
		$q->bindParam(':name', $this->name);
		$q->bindParam(':message', $this->message);
		$q->bindParam(':email', $this->email);
		$q->bindParam(':date_created', $this->date_created);
		$q->bindParam(':sent', $this->sent);

		return $q->execute();
	}

	public static function getAllIn10m() {
		$messages = array();
		$q = db::prepare('SELECT * FROM messages WHERE date_created > DATE_SUB(NOW(), INTERVAL 10 MINUTE) AND sent = 0');
		if($q->execute()) {
			while($row = $q->fetch(PDO::FETCH_ASSOC)) {
				$messages[] = new Messages($row);
			}
			return $messages;
		} else return false;
	}
}
