codelink = '?action=captcha';
msgs = {
	0: 'Неизвестная ошибка',
	1: 'Сообщение отправлено',
	2: 'Введен неверный E-mail адрес',
	3: 'Неверный код подтверждения',
	4: 'Заполните поле "ФИО"',
	5: 'Заполните поле "Сообщение"'
}
formDataPassed = {'action': 'save'};
$(function() {
	$("#form").on( "submit", function(event) {
		event.preventDefault();

		if(isEmpty(this['name'].value)) {
			errorMsg(this['name'], 1);
			return false;
		} else {
			errorMsg(this['name'], 0);
			formDataPassed['name'] = this['name'].value;
		}

		if(isEmpty(this['message'].value)) {
			errorMsg(this['message'], 1);
			return false;
		} else{
			errorMsg(this['message'], 0);
			formDataPassed['message'] = this['message'].value;
		}

		if(!validateEmail(this['email'].value)) {
			errorMsg(this['email'], 1);
			return false;
		} else {
			errorMsg(this['email'], 0);
			formDataPassed['email'] = this['email'].value;
		}

		captcha();
	});

	$(document).on('click', function() {
		$('#captcha').removeClass('show');
		$('#captchaimg').attr('src', '');
		$('#scode').val('');
		$('#popup').removeClass('error');
	});
	$('#popup').on('click', function() {
		event.preventDefault();
		return false;
	});
	$('#captchaimg').on('click', function() {
		event.preventDefault();
		$(this).attr('src',  codelink + '&t=' + (new Date().getTime()));
	});
});

function captcha() {
	$('#captcha').addClass('show');
	$('#captchaimg').attr('src', codelink);
}

function send() {
	formDataPassed['code'] = $('#scode').val();

	$.ajax({
		type: "POST",
		url: '/tst5432/',
		data: formDataPassed,
		success: success,
		dataType: 'json'
	});

	function success(response) {
		// console.log(response);
		if(response.status) {
			$('#captcha').removeClass('show');
			$('#popup2').html(msgs[response.status]).show();
			setTimeout(function() {
				$('#popup2').html('').hide();
			}, 3000);
		} else {
			if(response.errorCode == 3) {
				$('#cptmsg').html(msgs[response.errorCode]);
				$('#popup').addClass('error');
			} else
				$('#popup2').html(msgs[response.status]).show().addClass('error');
		}
	}
}

function errorMsg(el, error) {
	error ? $(el).parent().addClass('error') : $(el).parent().removeClass('error');
}

function isEmpty(str) {
	return (!str || 0 === str.length);
}

function validateEmail(email) {
	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
}