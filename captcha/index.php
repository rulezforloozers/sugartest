<?php
class CaptchaSecurityImages {
	var $font = '';
	var $br = 227;
	var $bg = 240;
	var $bb = 243;
	var $tr = 55;
	var $tg = 79;
	var $tb = 120;
	var $nr = 227;
	var $ng = 240;
	var $nb = 243;

	function __construct(){
		$this->font = 'captcha/fonts/primer.ttf';
	}

	function setrgb($value,$composite) {
		if ( ($value > 255) || ($value < 0 ) ) return;
		switch ($composite) {
			case 'br':
				$this->br = (int) $value;
				break;
			case 'bg':
				$this->bg = (int) $value;
				break;
			case 'bb':
				$this->bb = (int) $value;
				break;
			case 'tr':
				$this->tr = (int) $value;
				break;
			case 'tg':
				$this->tg = (int) $value;
				break;
			case 'tb':
				$this->tb = (int) $value;
				break;
			case 'nr':
				$this->nr = (int) $value;
				break;
			case 'ng':
				$this->ng = (int) $value;
				break;
			case 'nb':
				$this->nb = (int) $value;
				break;
		}
	}

	function _generateCode($characters) {
		/* list all possible characters, similar looking characters and vowels have been removed */
		$possible = '23456789bcdfghjkmnpqrstvwxyz';
		$code = '';
		$i = 0;
		while ($i < $characters) {
			$code .= substr($possible, mt_rand(0, strlen($possible)-1), 1);
			$i++;
		}
		return $code;
	}

	function generate($width='200',$height='50',$characters='7') {
		$code = $this->_generateCode($characters);
		/* font size will be 75% of the image height */
		$font_size = $height-4;
		$image = @imagecreate($width, $height) or die('Cannot initialize new GD image stream');
		/* set the colours */
		$background_color = imagecolorallocate($image, $this->br, $this->bg, $this->bb);
		$text_color = imagecolorallocate($image, $this->tr, $this->tg, $this->tb);
		$noise_color = imagecolorallocate($image, $this->nr, $this->ng, $this->nb);
		/* generate random dots in background */
		for( $i=0; $i<($width*$height)/3; $i++ ) {
			imagefilledellipse($image, mt_rand(0,$width), mt_rand(0,$height), 1, 1, $noise_color);
		}
		/* generate random lines in background */
		for( $i=0; $i<($width*$height)/150; $i++ ) {
			imageline($image, mt_rand(0,$width), mt_rand(0,$height), mt_rand(0,$width), mt_rand(0,$height), $noise_color);
		}
		/* create textbox and add text */
		$textbox = imagettfbbox($font_size, 0, $this->font, $code) or die('Error in imagettfbbox function');
		$x = ($width - $textbox[4])/2;
		$y = ($height - $textbox[5])/2;
		imagettftext($image, $font_size, 0, $x, $y, $text_color, $this->font , $code) or die('Error in imagettftext function');
		/* output captcha image to browser */
		header('Content-Type: image/jpeg');
		imagejpeg($image);
		imagedestroy($image);
		return $code;
	}
}
