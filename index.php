<?php
session_start();
require_once 'view.php';
require_once 'captcha/index.php';

class Controller
{
	public function form() {
		echo view('form');
	}

	public function save() {
		$_POST['captcha'] = $_SESSION['security_code'];
		$ch = curl_init();
		$url = 'http://pma.ll/tst5432/app2.php';
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $_POST);
		curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 25);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$output = curl_exec($ch);
		if (curl_error($ch)) {
			echo curl_error($ch);
		}
		curl_close($ch);

		$result = json_decode($output);
		if($result->status) unset($_SESSION['security_code']);

		echo $output;
	}

	public function captcha() {
		$captcha = new CaptchaSecurityImages();
		$code = $captcha->generate();
		$_SESSION['security_code'] = $code;
	}
}

$controller = new Controller();
if(isset($_REQUEST['action']) && !empty($_REQUEST['action'])) {
	$controller->{$_REQUEST['action']}();
} else $controller->form();

// phpinfo();