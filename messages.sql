-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 19, 2017 at 06:51 AM
-- Server version: 10.1.22-MariaDB
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test100`
--

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `email` varchar(255) NOT NULL,
  `date_created` datetime NOT NULL,
  `sent` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `name`, `message`, `email`, `date_created`, `sent`) VALUES
('00209D10-A9EE-412C-82BC-AAE1047E9509', 'User1', 'Message from User1', 'user1@rambler.ru', '2017-04-18 22:42:00', 0),
('09733400-2E70-4F6D-9CDE-80329EA88709', 'User13', 'Message from User13', 'user13@rambler.ru', '2017-04-18 23:01:19', 0),
('3C35C8DA-216E-41B5-BF66-9E12BB9E3F23', 'User1', 'Message from User1', 'user1@rambler.ru', '2017-04-18 22:19:05', 0),
('42709038-2C8C-4B06-9548-0228C9A88B10', 'User11', 'Message from User11', 'user11@rambler.ru', '2017-04-18 22:48:14', 0),
('69563211-A71A-48FD-8670-41389FA6864D', 'User1', 'Message from User1', 'user1@rambler.ru', '2017-04-18 19:09:44', 0),
('A876FB69-9D4D-4887-A81C-9DCAEE8338ED', 'User13', 'Message from User13', 'user13@rambler.ru', '2017-04-18 23:00:02', 0),
('AA77D372-ECFF-43A7-AFDE-74C4DDE0B860', 'User12', 'Message from User12', 'user12@rambler.ru', '2017-04-18 22:51:50', 0),
('ABD5F274-ED4F-42DA-B66F-189C6152F059', 'User1', 'Message from User1', 'user1@rambler.ru', '2017-04-18 22:25:13', 0),
('C6404879-7037-489D-9AE1-2B37610D1B31', 'User3', 'Message from User3', 'user3@rambler.ru', '2017-04-18 19:10:49', 1),
('C6F7A65A-1F44-421E-B535-0493C5277D46', 'User2', 'Message from User2', 'user2@rambler1.ru', '2017-04-18 19:45:37', 1),
('CA89516D-5E29-4137-9D33-84DF9F95E9B7', 'User5', 'Message from User5', 'user5@rambler.ru', '2017-04-18 18:28:54', 1),
('D2BBDEF9-BDE0-48A8-A392-865EC393F379', 'User1', 'Message from User1', 'user1@rambler.ru', '2017-04-18 22:24:20', 0),
('DEA649D1-2BF8-497F-8404-CE8409CAE303', 'User156', 'Message from User15', 'user15@rambler.ru', '2017-04-18 23:00:45', 0),
('DEFCAC80-C916-4BB4-9000-2FF7A0857923', 'User4', 'Message from User4', 'user4@rambler.ru', '2017-04-18 13:47:41', 0),
('FF79FD4D-D0A0-4B8A-BD0F-44A460713768', 'User1', 'Message from User1', 'user1@rambler.ru', '2017-04-18 13:44:47', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
