<?php
function view($view, $data = array()) {
	ob_start();
	require_once 'views/form.php';
	$tmpl = ob_get_contents();
	ob_end_clean();

	return $tmpl;
}