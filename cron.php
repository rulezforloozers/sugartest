<?php
require_once 'model.php';

/*
$ crontab -e
1 * * * * /path/to/cron.php
*/

function sendMail($msg, $tmpl = 0) {
	if($tmpl) {
		// template 1
		$subject = 'К сожалению, мы не можем вам помочь';
		$message = '%s, добрый день! Мы не можем вам ничем помочь, т.к. не работаем с клиентами %s.';
		$domain = substr(strrchr($msg->getEmail(), "@"), 1);
		$message = sprintf($message, $msg->getName(), $domain);
	} else {
		// template 2
		$subject = 'Всё отлично';
		$message = "%s, добрый день! Очень рады, что вы выбрали %s. Ваш запрос: \"%s\" будет обработан в ближайшее время. Удачи!";
		$domain = substr(strrchr($msg->getEmail(), "@"), 1);
		$message = sprintf($message, $msg->getName(), $domain, $msg->getMessage());
	}

	$to = $msg->getEmail();
	return mail($to, $subject, $message);
}

$emails = array('mail.ru', 'rambler.ru', 'yandex.ru');
if($messages = Messages::getAllIn10m()) {
	foreach($messages as $message) {
		$domain = substr(strrchr($message->getEmail(), "@"), 1);
		if(in_array($domain, $emails)) { // send template1
			if(sendMail($message, 1))
				$message->setSent(1);
		} else {  // send template2
			if(sendMail($message))
				$message->setSent(1);
		}
	}

	// print_r($messages);
}
