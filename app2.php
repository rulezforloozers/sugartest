<?php
require_once 'model.php';

$message = new Messages($_POST);
echo $message->save() ?
	json_encode(array('status' => 1)) :
	json_encode(array('status' => 0, 'errorCode' => $message->getErrorCode()));